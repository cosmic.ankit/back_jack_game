# back_jack_game

Contains API for black jack game with basic functionalities with only HIT/STAND action for users.

## Installation

Use the package manager [npm](https://nodejs.org/en/) to install node package.

To install the game 

```bash
npm install
```

## Usage

To run the app for dev 

```bash
 npm run dev
```

To run the test

```bash
 npm run test
```
## For API documentation please visit the following --  >[LINK](https://documenter.getpostman.com/view/3945565/SzfCSQyq?version=latest)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
