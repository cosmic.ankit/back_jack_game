const chai = require("chai");
const sinon = require("sinon");
const expect = chai.expect;
const faker = require("faker");
const game_service = require("../../../gamecore/interfaces/blackjackgme_interface");
//var connection = require('../../../connection/connection');
var MongoClient = require("mongodb").MongoClient;
describe("StartGameService", function() {
  describe("step1", function() {
    it("should create a new game", async function() {
       var db = await MongoClient.connect('mongodb://localhost:27017/BLACKJACK_GAME_DB',{ useNewUrlParser: true, useUnifiedTopology: true})
       // console.log(db);
       const stubValue = {
        db: db,
        username: faker.name.findName()
      };
      const gameresponse = await game_service.start_new_game({
          db:stubValue.db,
          username:stubValue.username
      });
      expect(gameresponse.username).to.equal(stubValue.username);
      if(gameresponse.iscomplete==false){
        describe("step2", function(){
          it("should make player take hit", async function(){
            let player_hitreposne=await game_service.hit_player({
              db:stubValue.db,
              username:stubValue.username,
              sessionid:gameresponse.sessionid
            });
            if(player_hitreposne.iscomplete==true){
             // console.log(1)
             it(`game finished with winner ${player_hitreposne.winner}`,function(){
               expect(player_hitreposne.iscomplete).to.equal(true);
             })
            }else{
             // console.log(2)
              describe("step3",function(){
                it("should make player take another hit",async function(){
                  let player_hitreposne2=await game_service.hit_player({
                    db:stubValue.db,
                    username:stubValue.username,
                    sessionid:gameresponse.sessionid
                  });
                  if(player_hitreposne2.iscomplete==true){
                    it(`game finished with winner ${player_hitreposne2.winner}`,function(){
                      expect(player_hitreposne2.iscomplete).to.equal(true);
                    })
                  }
                  else{
                    describe("step4",function(){
                      it("should make player choose to stand",async function(){
                        let player_standreposne=await game_service.stand_player({
                          db:stubValue.db,
                          username:stubValue.username,
                          sessionid:gameresponse.sessionid
                        });
            
                        expect(player_standreposne.playerstand).to.equal(true);

                        describe("step5",function(){
                          it("make the dealer take a hit at cards",async function(){
                            let hit_standreposne_delaer=await game_service.hit_dealer({
                              db:stubValue.db,
                              username:stubValue.username,
                              sessionid:gameresponse.sessionid
                            });
                            expect(stubValue.username).to.equal(hit_standreposne_delaer.username);
                            if(!hit_standreposne_delaer.iscomplete){

                              describe("step6",function(){
                                it("make dealer takes a stand this is the ultimate tep of the game",async function(){
                                  let stand_standreposne_delaer=await game_service.stand_dealer({
                                    db:stubValue.db,
                                    username:stubValue.username,
                                    sessionid:gameresponse.sessionid
                                  });
                                  it(`game finished with winner ${hit_standreposne_delaer.winner}`,function(){
                                    expect(stand_standreposne_delaer.iscomplete).to.equal(true);
                                  })

                                })
                              })
                            }
                            else{
                              it(`game finished with winner ${hit_standreposne_delaer.winner}`,function(){
                                expect(hit_standreposne_delaer.iscomplete).to.equal(true);
                              })
                            }
                          })
                        })
                      
                      })
                    })
                    
                  }
                })
              })
              
            }
          })
        });
        
      }
      else{
        it(`game finished with winner ${gameresponse.winner}`,function(){
          expect(gameresponse.iscomplete).to.equal(true);
        }) 
      }
    });
    
  });
});